@extends('layouts.app')
@section('content')


<h1>Add a New Prdocut</h1>
<form method = 'post' action="{{action('ProductController@store')}}">
{{csrf_field()}}

<div class = "form-group">
    <label for = "name">What is the product's name?</label>
    <input type= "text" class = "form-control" name= "name">
</div>
<div class = "form-group">
    <label for = "price">What is the product's price?</label>
    <input type= "text" class = "form-control" name= "price">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Add Product">
</div>

</form>
@endsection