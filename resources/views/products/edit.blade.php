@extends('layouts.app')
@section('content')
<h1>Edit a Product</h1>
<form method = 'post' action="{{action('ProductController@update', $product->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "name">Product's Name to Update:</label>
    <input type= "text" class = "form-control" name= "name" value = "{{$product->name}}">
</div>
<div class = "form-group">
    <label for = "price">Product's Price to Update:</label>
    <input type= "text" class = "form-control" name= "price" value = "{{$product->price}}">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Update Product">
</div>

</form>

@endsection