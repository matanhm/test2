<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Product; 
use App\User; 
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id=Auth::id();
        $products = Product::all(); 
        return view('products.index', ['products'=>$products], ['id'=>$id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('owner')) {
            abort(403,"Sorry you are not allowed to add products..");
        } 

        return view ('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product();
        $id = Auth::id();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->user_id = $id;
        $product->save();
        return redirect('products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('owner')) {
            abort(403,"Sorry you are not allowed to edit products..");
        } 
        $product = Product::find($id);
        $product->save();
        return view('products.edit', compact('product'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        $product = Product::find($id);
        if(!$product->user->id == Auth::id()) return(redirect('products'));
        $product -> update($request->except(['_token']));
        if($request->ajax()){
            $id1= Auth::id();
            $product->user_id =$id1;
            $product->save();
            if($product->status==0){
            $product->user_id=1;
            $product->save();
        }
            return Response::json(array('result'=>'success', 'status'=>$request->status),200);
        }


        //$product = Product::find($id);
        //$product -> update($request->all());
        return redirect('products');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('owner')) {
            abort(403,"Sorry you are not allowed to delete products..");
        } 
        $product = Product::find($id);
        $product->delete();
        return redirect('products');
    }

}
